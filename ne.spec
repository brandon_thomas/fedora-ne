Name:        ne-editor
Version:     3.0.1
Release:     1%{?dist}
Summary:     The nice editor

License:     GPLv3
Group:       Applications/Editors
URL:         http://http://ne.di.unimi.it/
Source0:     http://ne.di.unimi.it/ne-%{version}.tar.gz

Requires:      texinfo, ncurses
BuildRequires: ncurses-devel, make, bash, perl, texinfo, sed

%description 
ne is a free (GPL'd) text editor based on the POSIX standard that runs (we
hope) on almost any UN*X machine. ne is easy to use for the beginner, but
powerful and fully configurable for the wizard, and most sparing in its
resource usage. If you have the resources and the patience to use emacs or the
right mental twist to use vi then probably ne is not for you. However, being
fast, small, powerful and simple to use, ne is ideal for email, editing through
phone line (or slow GSM/GPRS) connections and so on. Moreover, the internal
text representation is very compact--you can easily load and modify very large
files.

%prep
%setup -q -n ne-3.0.1

%build
cd src
make NE_GLOBAL_DIR=%{_datadir}/ne %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ne/syntax
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ne/macros
mkdir -p $RPM_BUILD_ROOT%{_infodir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT%{_docdir}/ne-editor/html
install -m 755 src/ne $RPM_BUILD_ROOT%{_bindir}/ne
install -m 644 syntax/*.jsf $RPM_BUILD_ROOT%{_datadir}/ne/syntax
install -m 644 macros/* $RPM_BUILD_ROOT%{_datadir}/ne/macros
install -m 644 doc/ne.1 $RPM_BUILD_ROOT%{_mandir}/man1
install -m 644 doc/ne.info* $RPM_BUILD_ROOT%{_infodir}
rm INSTALL

%files
%{_bindir}/ne
%{_datadir}/ne
%{_mandir}/man1/ne.1*
%{_infodir}/ne.info*
%doc doc/html
%doc doc/ne.texinfo
%doc doc/ne.txt
%doc doc/ne.pdf
%doc README.md
%doc NEWS
%doc CHANGES
%license COPYING

%post
install-info %{_infodir}/ne.info.gz %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    install-info --delete %{_infodir}/ne.info.gz %{_infodir}/dir
fi

* Wed Fed 10 Brandon Thomas <bthomaszx@gmail.com>
- Added missing macros in.
- Added licence in.
- Fixed various things in installation.

* Thu Jan 28 Brandon Thomas <bthomaszx@gmail.com>
- Made various structural changes.
- Imported spec file from their website
  http://ne.di.unimi.it/ne-3.0.1-1.src.rpm
